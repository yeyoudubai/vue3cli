const path = require('path');
const {
    config
} = require('process');

function resolve(dir) {
    return path.join(__dirname, './', dir)
}
module.exports = {
    publicPath: '/vue3cli/',
    assetsDir: 'static',
    lintOnSave: false
};