import { createApp } from "vue";
import Root from "./App.vue";
import router from "./router";
import store from "./store";
import 'amfe-flexible/index.js' //移动端适配
// 插件
import group from '@/group' 
const app = createApp(Root)
/* 代替 Vue.prototype */
app.config.globalProperties.author='Vue技术交流组'

app.use(router)
    .use(store)
    .use(group)
    .mount("#app")

