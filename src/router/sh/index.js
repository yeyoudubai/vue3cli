
const sh = [{
    path: '/circle',
    name: 'circle',
    component: () =>
        import("@/views/circle")
},{
    path:'/tabbar',
    name:'tabbar',
    component: () =>
    import("@/views/tabbar")
}]

export default sh