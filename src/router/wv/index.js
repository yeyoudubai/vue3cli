const wv = [{
    path: '/button',
    name: 'button',
    component: () =>
        import("@/views/button")
}, {
    path: '/cell',
    name: 'Cell',
    component: () =>
        import('@/views/cell')
}, {
    path: '/notice',
    name: 'Notice',
    component: () =>
        import('@/views/notice')
}, {
    path: '/icon',
    name: 'Icon',
    component: () =>
        import('@/views/icon')
}, {
    path: '/layout',
    name: 'Layout',
    component: () =>
        import('@/views/layout')
}, {
    path: '/toast',
    name: 'Toast',
    component: () =>
        import('@/views/toast')
}]

export default wv