const lqc = [{
  path: '/switch',
  name: 'switch',
  component: () =>
    import("@/views/switch")
}, {
  path: '/overlay',
  name: 'overlay',
  component: () => import("@/views/overlay")
}];
export default lqc