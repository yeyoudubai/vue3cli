import {
  createRouter,
  createWebHashHistory
} from "vue-router"
import Home from "../views/Home.vue"
import wv from "./wv" //王炜
import zsf from './zsf' //张三丰
import md from './md' // 马迪
import lqc from './lqc' // 李乾程
import sh from './sh' //孙浩
import sk from './sk' //少恺
const routes = [{
    path: "/",
    name: "Home",
    component: () =>
      import("@/views/Home")
  },
  ...wv,
  ...zsf,
  ...md,
  ...lqc,
  ...sh,
  ...sk
];

const router = createRouter({
  history: createWebHashHistory(),
  routes
});

export default router;