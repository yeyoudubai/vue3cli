
import { renderSlot, watchEffect, childrens, reactive, render, defineComponent } from "vue";
import "./index.scss";
const row = defineComponent({
  props: {
    gutter: {
      type: [Number, String],
      default: 0
    },
    type: String,
    justify: String,
    align: String,
    tag: {
      type: String,
      default: "div"
    }
  },
  setup(props, { slots, attrs }) {
    const state = reactive({
      totalSpan: 0,
      groups: [[]],
      spaces: []
    });
    props.gutter &&
      (slots.default().forEach((item, index) => {
        if (item.props) {
          state.totalSpan += Number(item.props.span);
          if (state.totalSpan > 24) {
            state.groups.push([index]);
            state.totalSpan -= 24;
          } else {
            state.groups[state.groups.length - 1].push(index);
          }
        }
      }),
        state.groups.forEach(group => {
          var averagePadding = (props.gutter * (group.length - 1)) / group.length;
          group.forEach(function (item, index) {
            if (index === 0) {
              state.spaces.push({
                right: averagePadding
              });
            } else {
              var left = (props.gutter - state.spaces[item - 1].right).toFixed(2);
              var right = (averagePadding - left).toFixed(2);
              state.spaces.push({
                left: left,
                right: right
              });
            }
          });
        }),
        slots.default().forEach((item, index) => {
          item.children.default()[0].spaces = [state.spaces[index]];
          //console.log(item)
        }));

    return () => (
      <>
        <props.tag
          class="g-row"
          class={[
            props.type && `g-row-${props.type}`,
            props.justify && props.justify && `g-row-justify-${props.justify}`,
            props.align && props.align && `g-row-align-${props.align}`
          ]}
        >
          {renderSlot(slots, "default")}
        </props.tag>
      </>
    );
  }
})
export default row