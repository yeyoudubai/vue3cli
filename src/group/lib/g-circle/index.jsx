import {defineComponent} from "vue"
import './index.scss'
const circle = defineComponent({
  props: {
    type: {
      type: String,
      default: "default"
    },
    disabled: {
      type: Boolean,
      default: false
    },
    color:{
        type:String,
        default:"red"
    }
  },
  setup(props, { slots, emit }) {
    return () => (
      <div style={{background:props.color}}>测试circle464645</div>
    );
  }
})
export default circle