
import { renderSlot, defineComponent } from "vue";
import { useRouter } from "vue-router";
import "./index.scss";
const cell = defineComponent({
  props: {
    title: {
      //标题
      type: String,
      default: ""
    },
    value: {
      //内容
      type: String,
      default: ""
    },
    label: {
      // 内容描述
      type: String,
      default: ""
    },
    icon: {
      //左侧图标
      type: String,
      default: ""
    },
    isLink: {
      //显示右侧箭头
      type: Boolean,
      default: false
    },
    center: {
      //是否垂直居中
      type: Boolean,
      default: false
    },
    size: {
      type: String,
      default: ""
    },
    to: {
      type: String,
      default: null
    },
    url: {
      type: String,
      default: null
    },
    replace: {
      type: Boolean,
      default: false
    }
  },
  setup(props, { slots }) {
    const router = useRouter();
    const go = function () {
      if (props.to) {
        !props.replace
          ? router.push({ path: props.to })
          : router.replace({ path: props.to });
      }
      if (props.url) window.location.href = props.url;
    };
    return () => (
      <div
        role="button"
        class="g-cell"
        class={props.center && `g-cell-center`}
        class={props.size && `g-cell-${props.size}`}
        class={(props.url || props.to) && `g-tab`}
        onClick={go}
      >
        {props.icon && !slots.icon ? (
          <i class={`g-icon g-cell-left-icon g-icon-${props.icon}`}></i>
        ) : (
            renderSlot(slots, "icon")
          )}

        {props.title && !slots.title ? (
          <div class="g-cell-title">
            <span>{props.title}</span>
            {props.label && !slots.label && (
              <div class="g-cell-label">{props.label}</div>
            )}
            {slots.label && renderSlot(slots, "label")}
          </div>
        ) : (
            <div class="g-cell-title">
              <div>{renderSlot(slots, "title")}</div>
              {props.label && !slots.label && (
                <div class="g-cell-label">{props.label}</div>
              )}
              {slots.label && renderSlot(slots, "label")}
            </div>
          )}

        {slots.default ? (
          <div
            class="g-cell-value"
            class={!props.title && !slots.title && `g-cell-value-alone`}
          >
            {renderSlot(slots, "default")}
          </div>
        ) : (
            props.value && (
              <div
                class="g-cell-value"
                class={!props.title && !slots.title && `g-cell-value-alone`}
              >
                <span>{props.value}</span>
              </div>
            )
          )}

        {props.isLink && (
          <i class="g-icon g-cell-right-icon g-icon-arrow-right"></i>
        )}
        {slots.extra && renderSlot(slots, "extra")}
      </div>
    );
  }
})
export default cell