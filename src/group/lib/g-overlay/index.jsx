
import { defineComponent } from "vue"
import "./index.scss";
const overlay = defineComponent({
  props: {
    modelValue: {
      type: Boolean,
      default: false
    },
    "z-index": {
      type: [Number, String],
      default: 1
    },
    duration: {
      type: [Number, String],
      default: 0.3
    },
    "class-name": {
      type: String
    },
    "custom-style": {
      type: Object
    },
    "lock-scroll": {
      type: [Boolean, String],
      default: true
    }
  },
  setup(props, { attrs, slots, emit }) {
    console.log(1211)
    const status = props.modelValue

    return () => (
      <div class="g-overlay"
        style={{ display: props.modelValue === true ? 'flex' : 'none' }}
        style={{ animationDirection: props.duration }}>
        test
      </div>
    )
  },
  data() {
    return {
      testValue: 123
    }
  },
  watch: {
    modelValue() {
      console.log(this.testValue)
    }
  }
})
export default overlay