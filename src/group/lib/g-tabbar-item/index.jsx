import { defineComponent, getCurrentInstance, watchEffect } from 'vue'
import { useRouter } from 'vue-router'
import './index.scss'

const tabbarItem = defineComponent({
    props: {
        icon: String,
        to: String
    },
    setup(props, { slots, emit, attrs }) {
        const { ctx } = getCurrentInstance()
        const router = useRouter()
        const routerGo = () => {
            /* 双向绑定控制视图 */
            ctx._.parent.emit('update:active', slots.default()[0].key)
            /* 控制路由跳转 */
            props.to && ctx._.parent.props.route && router.push({ path: props.to })
        }
        return () => (
            <div class="g-tabbar-item"
                onClick={routerGo}
                class={{
                    'active': ctx._.parent.props.active == slots.default()[0].key
                }}
            >
                <g-icon class="g-icon" name={props.icon} />
                <h3>{slots.default()}</h3>
            </div >
        );
    }
})

export default tabbarItem 