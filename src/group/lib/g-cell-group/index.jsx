import { renderSlot,defineComponent } from "vue";
import "./index.scss"; 
const cellGroup = defineComponent({
  props: {
    title: {
      type: String,
      default: ""
    }
  },
  setup(props, { slots }) {
    return () => (
      <div>
        {props.title&&(<div class="g-cell-group-title">{props.title}</div>)}
        
        <div class="g-cell-group g-hairline-top-bottom">
          {renderSlot(slots, "default")}
        </div>
      </div>
    ); 
  }
})
export default cellGroup