
import { ref, reactive,defineComponent } from "vue";
import "./index.scss";
const col = defineComponent({
props: {
    span: [String, Number],
    offset: [String, Number],
    tag: {
      type: String,
      default: "div"
    }
  },
  setup(props, { slots, attrs }) {
    const state = reactive({
      styleModel: {},
      child: null
    });
    slots.default()[0].spaces &&
      ((state.child = slots.default()[0].spaces[0]),
      Object.keys(state.child).forEach((item, index) => {
        state.styleModel[`padding-${item}`] =
          Object.values(state.child)[index] + "px";
      }));

    return () => (
      <>
        <props.tag
          class={`g-col g-col-${props.span}`}
          class={props.offset && `g-col-offset-${props.offset}`}
          style={state.styleModel}
        >
          {slots.default()}
        </props.tag>
      </>
    );
  }
})
export default col
