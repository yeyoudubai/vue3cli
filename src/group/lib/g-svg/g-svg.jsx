
import { computed, defineComponent } from "vue";
const svg = defineComponent({
  props: {
    iconClass: {
      type: String,
      required: true
    },
    className: {
      type: String,
      default: ""
    }
  },
  setup(props) {
    const iconName = computed(() => `#icon-${props.iconClass}`);
    const svgClass = computed(() => {
      if (props.className) {
        return "svg-icon " + props.className;
      }
    });
    return () => (
      <svg class="svg-icon" class={svgClass.value} aria-hidden="true">
        <use href={iconName.value} />
      </svg>
    );
  }
})

export default svg