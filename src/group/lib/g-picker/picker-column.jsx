import { defineComponent ,ref ,withModifiers,watch,computed,getCurrentInstance} from "vue"
export default defineComponent({
	props: {  
		changeIndex:{
	  		type:Function
	  	},
		unit:{
			type:[String],
			default:"px"
		},
	  	index:{
	  		type:[Number],
	  		default: 0
	  	},
	  	data:{
	  		type: [Array],
	  		default: []
	  	}, 
	  	readOnly:{
	  		default:false,
	  		type:Boolean
	  	},
	  	visibleItemCount:{
	  		type:[Number,String],
	  		default:6
	  	},
	  	itemHeight:{
	  		type:[Number,String],
	  		default:44
	  	},
	  	swipeDuration:{
	  		type:[Number,String],
	  		default:200
	  	}
  	},
  	setup(props, { slots, emit }){
  		var moverValue=80;	
  		var {ctx} = getCurrentInstance(); 
		var index=ref(props.index); 
		var catchPoint=ref({
			_index:0,
			y:0,
			lastY:0,
			isDraw:false,
			isEnd:false
		}); 
		ctx.setIndex=function(_index){
  			index.value=_index;
  		}
  		ctx.setName=function(name){
  			for(var i=0;i<props.data.length;i++){
  				if(props.data[i].name==name){
  					ctx.setIndex(i);
  					return i;
  				}
  			}
  		}
  		var events={
  			translatePoint:function(){
				var itemHeight=parseInt(props.itemHeight); 
		  		var showSize=props.visibleItemCount;  
	  			var result=(itemHeight*showSize/2)-itemHeight/2;  
		  		result-=index.value*itemHeight;
		  		return (result)+props.unit;
  			},
  			translate3dEnd:function(){
  				catchPoint.value.isEnd=false;	
  			},
  			mover:function(){
		  		if(props.readOnly){
		  			return;
		  		}     
		  		if(event.type=="touchend"){
			  		index.value=parseInt(catchPoint.value._index-(catchPoint.value.lastY-catchPoint.value.y)/moverValue);
			  		if(index.value<0){
			  			index.value=0;
			  		}else if(index.value>=props.data.length){
			  			index.value=props.data.length-1;
			  		}
			  		if(props.changeIndex){
			  			props.changeIndex(index.value);
			  		}
			  		catchPoint.value.isEnd=true;  
		  		//开始
		  		}else if(event.type=="touchstart"){
		  			catchPoint.value._index=index.value;
		  			catchPoint.value.y=event.touches[0].clientY;
	  				catchPoint.value.isEnd=false;
		  		//移动中
		  		}else {
		  			catchPoint.value.isEnd=false; 
		  			catchPoint.value.lastY=event.touches[0].clientY;
		  			index.value=catchPoint.value._index-(catchPoint.value.lastY-catchPoint.value.y)/moverValue;
		  		}  
	  		}
  		};
  		
  		
  		
  		
  		var gethtml=function(){
  			return (
	    	<div  class="g-picker-column" 
	    		onTouchstart={events.mover}
	    		onTouchmove={events.mover}
	    		onTouchend= {events.mover} 
	    		style={{ 
	    			height:(parseInt(props.itemHeight)*props.visibleItemCount+props.unit)
	    		}}
	    	> 
	    		<div
	    		onTransitionend={events.translate3dEnd} 
	    		style={{
    			    transform:"translate3d(0px, "+events.translatePoint()+", 0px)",
    			   'transition-property':catchPoint.value.isEnd?'all':'none',
                	'transition-duration':catchPoint.value.isEnd?props.swipeDuration+'ms':'0ms'
	    		}}>
	    		<ul>
	    			{
	    				props.data.map((item,_indx)=>{
	    					return <li class="g-picker-column__item" 
	    					style={{
	    						height:parseInt(props.itemHeight)+props.unit
	    					}} 
	    					onClick={()=>{index.value=_indx; props.changeIndex?props.changeIndex(_indx):null;}}
	    					>{item.name}</li>
	    				})
	    			}
	    		</ul>
	    		</div>
	    	</div> 
	   		 );
  		}
  		return gethtml; 
  	}
})
