import { ref ,defineAsyncComponent,watch,getCurrentInstance,defineComponent} from "vue"; 
import "./index.scss";

export default defineComponent({
	components:{
		'pickerColumn':defineAsyncComponent (()=>import('./picker-column.jsx') )
	},
  	props: { 
	  	defaultIndex:{
			type:[Number,String],
			default:0
	  	},
	  	//字段	需要单独做处理 
	  	columns:{
	  		type:Array,
	  		default: []
	  	},
	  	// 标题
	  	title:{
	  		type:[String],
	  		default:"标题"
	  	},
	  	//确定按钮
	  	confirmButtonText:{
	  		type:String,
	  		default:"确定"
	  	},
	  	//取消按钮
	  	cancelButtonText:{
	  		type:String,
	  		default:"取消"
	  	},
	  	//
	  	loading:{
	  		type:Boolean,
	  		default:false,
	  	},
	  	showToolbar:{
	  		type:Boolean,
	  		default:true
	  	},
	  	toolbarPosition:{
	  		type:String,
	  		default:"top"	
	  	},
	  	visibleItemCount:{
	  		type:[Number,String],
	  		default:5
	  	},
	  	itemHeight:{
	  		type:[Number,String],
	  		default:"44px"
	  	},
	  	swipeDuration:{
	  		type:[Number,String],
	  		default:200
	  	},
	  	confirm:{
	  		type:Function,
	  	},
	  	change:{
	  		type:Function
	  	},
	  	cancel:{
	  		type:Function
	  	}
  },
  setup(props, context) {
  	const {ctx} = getCurrentInstance(); 
  	var unit=(function(){
  		if(typeof(props.itemHeight)=="string"){
			if(props.itemHeight.lastIndexOf("px")!=-1){
				return "px";
			}else if(props.itemHeight.lastIndexOf("rem")!=-1){
				return "rem";
			}else if(props.itemHeight.lastIndexOf("vw")!=-1){
				return "vw";
			}
		}
		return "px";
  	})();
  	var changeIndexValue=ref([]);
    var data=ref([]);  
    //通过下标获取组件对象
    var getColumn=function(index){ 
    	return ctx._.refs['picker-column-'+index].$.refs['picker-column-'+index];
    }
    //获取所有列选中的值
    ctx.getValues=function(){
    	var result=[];
    	for(var i=0;i<changeIndexValue.value.length;i++){
    		result.push(data.value[i][changeIndexValue.value[i]].name); 
    	}
    	return result;
    }
    //设置所有列选中的值
    ctx.setValues=function(values){
    	if(values.length==data.value.length){
    		for(var i=0;i<values.length;i++){
    			var result=getColumn(i).setName(values[i]);
    			if(result!=undefined){
    				changeIndexValue.value[i]=result;
    			}
    		}
    	}
    }
    //获取所有列选中值对应的索引
    ctx.getIndexes=function(){
    	var result=[];
    	for(var i=0;i<changeIndexValue.value.length;i++){
    		result.push(changeIndexValue.value[i]); 
    	}
    	return result;
    }
    //设置所有列选中值对应的索引
    ctx.setIndexes=function(indexes){
    	changeIndexValue.value=indexes;
    	for(var i=0;i<changeIndexValue.value.length;i++){
    		getColumn(i).setIndex(changeIndexValue.value[i]);
    	}
    }
    //获取对应列选中的值
    ctx.getColumnValue=function(columnIndex){
    	if(columnIndex<changeIndexValue.value.length){
    		return data.value[columnIndex][changeIndexValue.value[columnIndex]]
    	}
    }
    //设置对应列选中的值
    ctx.setColumnValue=function(columnIndex, value){
    	if(columnIndex<changeIndexValue.value.length){
    		var result=getColumn(columnIndex).setName(value);
    		if(result!=undefined){
				changeIndexValue.value[columnIndex]=result;
			}
    	}
    }
    //获取对应列选中项的索引
    ctx.getColumnIndex=function(columnIndex){
    	if(columnIndex<changeIndexValue.value.length){
    		return changeIndexValue.value[columnIndex];
    	}
    }
    //设置对应列选中项的索引
	ctx.setColumnIndex=function(columnIndex, optionIndex){
    	if(columnIndex<changeIndexValue.value.length){
    		changeIndexValue.value[columnIndex]=optionIndex;
    		getColumn(columnIndex).setIndex(optionIndex);
    	}
    }
	//获取对应列中所有选项
	ctx.getColumnValues=function(columnIndex){
    	var result=[];
    	if(columnIndex<changeIndexValue.value.length){
    		for(var i=0;i<data.value[columnIndex].length;i++){
    			result.push(data.value[columnIndex].name);
    		}
    	}
    	return result;
    }
	//设置对应列中所有选项
	ctx.setColumnValues=function(columnIndex, values){
    	if(columnIndex<changeIndexValue.value.length){
    		//不做树形结构的处理
    		var list=[];
    		for(var i=0;i<values.length;i++){
    			list.push({
    				name:values[i]
    			});
    		}
    		data.value[columnIndex]=list;
    	}
    }
    
    var events={
    	confirm:function(){
    		var result=ctx.getIndexes();
    		if(result.length==1){
    			context.emit("confirm",result[0]);
    		}else{
    			context.emit("confirm",result);
    		}
    	},
    	cancel:function(){
    		var result=ctx.getIndexes();
    		if(result.length==1){
    			context.emit("cancel",result[0]);
    		}else{
    			context.emit("cancel",result);
    		}
    	},
    	change:function(){
    		var result=ctx.getIndexes();
    		if(result.length==1){
    			context.emit("change",result[0]);
    		}else{
    			context.emit("change",result);
    		}
    	},
    	init:function(){
    		data.value=getData(props.columns);
    		changeIndexValue.value=[];
    		for(var i=0;data.value.length;i++){
    			var item=data.value[i];
    			var nubmerIndex=0;
    			if(item.length>0&&item[0].children){
    				while(item){
    					nubmerIndex=0;
    					for(var q=0;q<item.length;q++){
    						if(item[i].index){
    							nubmerIndex=item.index;
    						}
    					}
    					if(item[nubmerIndex].children&&
    						item[nubmerIndex].children.length>nubmerIndex){
    						item=item[nubmerIndex].children;
    					}else{
    						item=null;
    					}
						changeIndexValue.value.push(nubmerIndex);
    				}
    				break;
    			}else{
	    			for(var q=0;q<item.length;q++){
	    				if(item[i].index){
	    					nubmerIndex=item[i].index;
	    					break;
	    				}
	    			}	
	    			changeIndexValue.value.push(nubmerIndex);
    			}
    		}
    	},
    	changIndex:function(item,index,_index){ 
			changeIndexValue.value[_index]=index; 
	    	if(item[index].children&&item[index].children.length>0){ 
	    		for(var i=_index+1;i<changeIndexValue.value.length;i++){
    				changeIndexValue.value[i]=0; 
	    			var column= getColumn(i);
	    			column.setIndex(0); 
	    		} 
	    	}
	    	events.change();
    	}
    	
    }; 
    var getData=function(data){
    	var result=[];
		if(data&&data.length>0){
			var item=data[0];
			if(typeof(item)=="object"){ 
				if(item.children){
					var toTree=function(data){
						var result=[];
						for(var i=0;i<data.length;i++){
							var item={
								name:data[i].text
							};
							if(data[i].children){
								item.children=toTree(data[i].children);
							}
							result.push(item);
						}
						return result;
					}
					var value=toTree(data);
					result.push(value);
					if(value.length>0){
						var item=value[0];
						while(item&&item.children){
							result.push(item.children);
							if(item.children.length>0){
								item=item.children[0];	
							}else{
								item=null;
							}
						 } 
					} 
				}else if(item.values instanceof Array){   
					for(var i=0;i<data.length;i++){
						var item=[]; 
						for(var q=0;q<data[i].values.length;q++){
							var value={
								name:data[i].values[q]
							};
							if(data[i].defaultIndex==q){
								value.index=data[i].defaultIndex;
							}
							item.push(value);
						}
						result.push(item);
					}
				} 
			}else if(typeof(item)=="string"){
				for(var i=0;i<data.length;i++){
	    			var item={
	    				name:data[i]
	    			};
	    			if(props.defaultIndex==i){
	    				item.index=props.defaultIndex;
	    			}
	    			result.push(item);
	    		}
				result=[result];
			}
		}
		return result;
    }  
    events.init();
    var	html={
    	head:function(){
    		return (
    			<div class="g-picker__toolbar">
    				<button type="button"
    				onClick={events.cancel}
    				class="g-picker__cancel">
    					{props.cancelButtonText}
    				</button>
    				<div class="g-ellipsis g-picker__title"
    	 
    				>
    					{props.title}
    				</div>
    				<button type="button" class="g-picker__confirm"
						onClick={events.confirm}
    				>
    					{props.confirmButtonText}
    				</button>
    			</div>
    		)
    	},
    	body:function(){
    		return (
		    	<div class="g-picker"> 
			    	{
			    		props.toolbarPosition=="top"?(html.head()):""
			    	} 
			    	<div class="g-picker__columns" >
							{
								data.value.map((item ,_index)=>{
						      return <pickerColumn 
						      ref={'picker-column-'+_index}
						      data={item}  
						      unit={unit}
						      item-height={props.itemHeight}  
						      swipe-duration={props.swipeDuration} 
						      visible-item-count={props.visibleItemCount}
						      index={changeIndexValue.value[_index]} 
						      change-index={(index)=>{events.changIndex(item,index,_index)}}
						      
						      ></pickerColumn>
						   }) 
							} 
							<div class="g-picker__bg">
			    			<i class="g-picker__line"
			    				style={{
			    					height:(parseInt(props.itemHeight)+unit) 
			    				}}
			    			></i>
			    		</div>
			    	</div> 
			     	{
			    		props.toolbarPosition=="bottom"?(html.head()):""
			    	}  
			    	{
			    		props.loading?(<div class="g-picker__mask"><div>加载中</div></div>):""
			    	}
		    	</div>
		    	)
    	}
    }; 
    return html.body;
  }
});

