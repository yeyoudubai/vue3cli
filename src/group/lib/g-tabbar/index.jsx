import { defineComponent } from 'vue'
import './index.scss'

const tabbar = defineComponent({
  props: {
    active: [String, Number],
    route:Boolean
  },
  setup(props, { slots, emit }) {
    return () => (
      <div class="g-tabbar">
        {slots.default().map((item,index)=>{
          item.children.default()[0].key=index
          return item
        })}
      </div>
    );
  }
})

export default tabbar 