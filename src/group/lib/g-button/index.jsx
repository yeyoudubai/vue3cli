
import { ref,defineComponent } from "vue";
import { useRouter } from "vue-router";
import "./index.scss";

const button = defineComponent({
   props: {
    type: {
      type: String,
      default: "default"
    },
    radius: {
      type: Boolean,
      default: false
    },
    disabled: {
      type: Boolean,
      default: false
    },
    size: {
      type: String,
      default: "normal"
    },
    plain: {
      type: Boolean,
      default: false
    },
    hairline: {
      type: Boolean,
      default: false
    },
    loading: {
      type: Boolean,
      default: false
    },
    loadingText: {
      type: String,
      default: null
    },
    loadingType: {
      type: String,
      default: "circular"
    },
    icon: {
      type: String,
      default: null
    },
    to: {
      type: String,
      default: null
    },
    url: {
      type: String,
      default: null
    },
    replace: {
      type: Boolean,
      default: false
    },
    color: {
      type: String,
      default: ""
    }
  },
  setup(props, { slots, emit }) {
    const router = useRouter();
    const go = function() {
      if (props.to) {
        !props.replace
          ? router.push({ path: props.to })
          : router.replace({ path: props.to });
      }
      if (props.url) window.location.href = props.url;
    };
    return () => (
      <button
        style={{ background: props.color }}
        disabled={props.disabled || props.loading}
        class={`group-button-${props.size} group-button group-button-${props.type}`}
        class={{
          radius: props.radius,
          "group-button-disabled": props.disabled,
          "group-button-plain": props.plain,
          "group-button-hairline": props.hairline,
          "group-button-hairline-surround": props.hairline,
          "group-button-color": props.color
        }}
        onClick={go}
      >
        {props.loading ? (
          /*  加载中的按钮  */
          <div class="g-loding">
            <span
              v-show={props.loadingType === "circular"}
              class="g-loading-spinner-circular g-loading-spinner"
            >
              <svg viewBox="25 25 50 50" class="g-loading-circular">
                <circle cx="50" cy="50" r="20" fill="none" />
              </svg>
            </span>

            <g-icon
              v-show={props.loadingType === "spinner"}
              class="g-loading-spinner g-loading-spinner-spinner"
              name="jiazai"
              style="width:auto;height:auto"
            />
            <span class="g-btn-loding-text" v-show={props.loadingText}>
              {props.loadingText}
            </span>
          </div>
        ) : (
          /*  普通按钮  */
          <div class="group-button-content waves" v-show={!props.loading}>
            {props.icon &&
              (props.icon.indexOf("http") != ~0 ? (
                <img class="g-btn-img" src={props.icon} />
              ) : (
                <g-icon class="g-btn-icon g-icon" name={props.icon} />
              ))}
            <span class="group-button-text">
              {slots.default && slots.default()}
            </span>
          </div>
        )}
      </button>
    );
  }
})
export default button