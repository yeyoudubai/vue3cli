
import { defineComponent } from "vue";
import "./index.scss";
const switchs = defineComponent({
props:{
        type:{
            type:String,
            default:"default"
        },
        modelValue: {
            type: Boolean,
            default: '默认'
        },
        disabled: {
            type: Boolean,
            default: false
        },
        size: {
            type: [String, Number],
            default: 64
        }
    },
    setup(props,{slots,emit}){
        console.log(props)
        const change =  () => {
            emit('update:modelValue', !props.modelValue)
        }
        return()=>(
            <div class="g-switch"
                class={ props.disabled === true ? 'g-switch-disabled' : '' }
                style={{ 'font-size': typeof props.size === 'number' ? `${props.size}px` : props.size }}
                g-checked={props.modelValue}
                onClick={props.disabled === true ? '' : change}
                class={ props.modelValue === true ? 'g-switch--checked' : '' }>
                <div class="g-switch__node-box"
                    class={ props.modelValue === true ? 'g-switch__node--checked' : 'g-switch__node' }></div>
            </div>
        )
    }
})
export default switchs