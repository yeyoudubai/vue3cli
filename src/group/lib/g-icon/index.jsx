
import { reactive, nextTick, renderSlot, ref, watch,defineComponent } from "vue";
import "./index.scss";
const icon = defineComponent({
  props: {
    name: String
  },
  setup(props) {
    return () => <i class={`g-icon  g-icon-${props.name}`}></i>;
  }
})
export default icon

