import { reactive, nextTick, renderSlot, ref, watch, defineComponent } from "vue";
import "./index.scss";
const notice = defineComponent({
  props: {
    delay: {
      type: [Number, String],
      default: 1
    },
    scrollable: {
      type: Boolean,
      default: null
    },
    speed: {
      type: [Number, String],
      default: 80
    },
    text: String,
    leftIcon: String
  },
  setup(props) {
    const data = reactive({
      offset: 0,
      duration: 0,
      wrapWidth: 0,
      contentWidth: 0
    });
    const reset = function () {
      //重置
      data.offset = 0;
      data.duration = 0;
      data.wrapWidth = 0;
      data.contentWidth = 0;
    };
    /* 分割线 */
    const wrap = ref(null);
    const content = ref(null);
    /* 分割线 */
    const delay =
      props.delay != undefined && typeof props.delay != null
        ? props.delay * 1000
        : 0;
    const onTransitionEnd = function () {
      data.offset = data.wrapWidth;
      data.duration = 0;
      setTimeout(() => {
        data.offset = -data.contentWidth;
        data.duration = (data.contentWidth + data.wrapWidth) / props.speed;
      }, 0);
    };
    const start = function () {
      reset();
      setTimeout(function () {
        if (!wrap.value || !content.value || props.scrollable === false) {
          return;
        }
        var wrapWidths = wrap.value.getBoundingClientRect().width;
        var contentWidths = content.value.getBoundingClientRect().width;
        if (props.scrollable || contentWidths > wrapWidths) {
          data.offset = -contentWidths;
          data.duration = contentWidths / props.speed;
          data.wrapWidth = wrapWidths;
          data.contentWidth = contentWidths;
        }
      }, delay);
    };
    nextTick(() => {
      start();
    });
    return () => (
      <div role="alert" class="g-notice-bar" ref={wrap}>
        {props.leftIcon && (
          <i
            class={`g-icon g-icon-${props.leftIcon} g-notice-bar-left-icon`}
          ></i>
        )}

        <div role="marquee" class="g-notice-bar-wrap">
          <div

            onTransitionend={onTransitionEnd}
            class="g-notice-bar-content"
            ref={content}
            style={{
              transform: data.offset ? "translateX(" + data.offset + "px)" : "",
              transitionDuration: data.duration + "s",

            }}
          >
            <span>
              {props.text}
            </span>

          </div>
        </div>
      </div>
    );
  }
})
export default notice