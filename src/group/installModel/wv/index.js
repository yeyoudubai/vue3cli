import gButton from '../../lib/g-button'
import gCell from '../../lib/g-cell'
import gCellGroup from '../../lib/g-cell-group'
import gNotice from '../../lib/g-notice'
import gIcon from '../../lib/g-icon'
import gRow from '../../lib/g-row'
import gCol from '../../lib/g-col'
export default {
    gButton,
    gCell,
    gCellGroup,
    gNotice,
    gIcon,
    gRow,
    gCol
}