import gSwitch from '../../lib/g-switch'
import gOverlay from '../../lib/g-overlay'

export default {
  gSwitch,
  gOverlay
}