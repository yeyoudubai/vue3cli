import gCircle from '../../lib/g-circle'
import gTabbar from '../../lib/g-tabbar'
import gTabbarItem from '../../lib/g-tabbar-item'
export default {
    gCircle,
    gTabbar,
    gTabbarItem
}