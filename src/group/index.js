import './index.css'
import wv from './installModel/wv' //小C
import sh from './installModel/sh' //小布
import sk from './installModel/sk' //小k
import lqc from './installModel/lqc' //小L
import gIcon from './lib/g-icon'
import './lib/g-svg/index'
/* 
  @author 小C
  @describe 组件json，格式：'组件名'：导入的组件

*/
const componets = {
  ...wv,
  ...sh,
  ...sk,
  ...lqc
}



export default {
  install: ({
    component,
    config,
    directive,
    mixin,
    mount,
    provide,
    unmount,
    use,
    version
  }, options) => {
    for (let key in componets) {
      component(key, componets[key])
    }
    console.log("%c " + 'VUE技术交流组版权所有', "color: " + "red")
  }
}