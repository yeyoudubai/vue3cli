class Toast {
    show(title) {
        this.crateDom({
            title
        })
    }
    crateDom({
        title,
        time = 2000
    }) {
        const parentVnode = document.getElementsByTagName('body')[0];
        const div = document.createElement('div');
        div.className = "g-toast g-toast-text g-toast-middle"
        div.innerText = title
        parentVnode.appendChild(div)
        setTimeout(() => {
            parentVnode.removeChild(div)
        }, time)
    }
}
export default Toast